# Jailbreaking-Jetbrains
## Jetbrains全家桶破解
众所周知，Jetbrains的软件都比较好用，但是对于一些资金上比较紧张的人来说，购买还是有些问题。
但不管怎么样，有能力尽量支持正版!!!   
***
## 感谢制作补丁的大佬[知了](https://github.com/pengzhile)!    
使用方法很简单，把“jetbrains-agent-latest.zip”拖入就行，具体看[博客内容](https://zhile.io/2018/08/25/jetbrains-license-server-crack.html)和readme.最新补丁理论上支持以下版本.     
***
## 支持版本
在以下IDE版本测试可成功激活（v3.2.0）：   
IntelliJ IDEA 2020.1及以下    
AppCode 2019.3.7及以下    
CLion 2019.3.5及以下    
DataGrip 2020.1及以下    
GoLand 2020.1及以下    
PhpStorm 2019.3.4及以下    
PyCharm 2020.1及以下    
Rider 2019.3.4及以下     
RubyMine 2019.3.4及以下     
WebStorm 2020.1及以下     

## 作者因特殊原因停止项目. 这只是备份, 以后不会更新.                   

